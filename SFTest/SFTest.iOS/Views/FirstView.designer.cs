﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace SFTest.iOS.Views
{
    [Register ("FirstView")]
    partial class FirstView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Syncfusion.SfChart.iOS.SFChart ChestTempGraph { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Syncfusion.SfChart.iOS.SFChart EcgGraph { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Syncfusion.SfChart.iOS.SFChart FootTempGraph { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Syncfusion.SfChart.iOS.SFChart IrGraph { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Syncfusion.SfChart.iOS.SFChart RedGraph { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Syncfusion.SfChart.iOS.SFChart ScgGraph { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton StartScanning { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ChestTempGraph != null) {
                ChestTempGraph.Dispose ();
                ChestTempGraph = null;
            }

            if (EcgGraph != null) {
                EcgGraph.Dispose ();
                EcgGraph = null;
            }

            if (FootTempGraph != null) {
                FootTempGraph.Dispose ();
                FootTempGraph = null;
            }

            if (IrGraph != null) {
                IrGraph.Dispose ();
                IrGraph = null;
            }

            if (RedGraph != null) {
                RedGraph.Dispose ();
                RedGraph = null;
            }

            if (ScgGraph != null) {
                ScgGraph.Dispose ();
                ScgGraph = null;
            }

            if (StartScanning != null) {
                StartScanning.Dispose ();
                StartScanning = null;
            }
        }
    }
}