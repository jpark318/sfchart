﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using SFTest.Core.Models;
using SFTest.Core.ViewModels;
using Syncfusion.SfChart.iOS;
using UIKit;

namespace SFTest.iOS.Views {
    [MvxFromStoryboard]
    public partial class FirstView : MvxViewController {
        public FirstView(IntPtr handle) : base(handle) {
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();
            var set = this.CreateBindingSet<FirstView, FirstViewModel>();
			
            //Adding Primary Axis for the Chart.
            SFNumericalAxis primaryAxis = new SFNumericalAxis();
            //primaryAxis.Minimum = new NSNumber(0);
            //primaryAxis.Maximum = new NSNumber(2000);

            //Adding Secondary Axis for the Chart.
            SFNumericalAxis secondaryAxis = new SFNumericalAxis() {
            };
	        EcgGraph.PrimaryAxis = new SFNumericalAxis();
	        ScgGraph.PrimaryAxis = new SFNumericalAxis();
	        RedGraph.PrimaryAxis = new SFNumericalAxis();
	        IrGraph.PrimaryAxis = new SFNumericalAxis();
	        FootTempGraph.PrimaryAxis = new SFNumericalAxis();
	        ChestTempGraph.PrimaryAxis = new SFNumericalAxis();

	        EcgGraph.SecondaryAxis = new SFNumericalAxis();
	        ScgGraph.SecondaryAxis = new SFNumericalAxis();
	        RedGraph.SecondaryAxis = new SFNumericalAxis();
	        IrGraph.SecondaryAxis = new SFNumericalAxis();
	        FootTempGraph.SecondaryAxis = new SFNumericalAxis();
	        ChestTempGraph.SecondaryAxis = new SFNumericalAxis();

			Collection<SFFastLineSeries> sFFastLineSeries = new Collection<SFFastLineSeries>();

            for (int i = 0; i < 6; i++) {
                sFFastLineSeries.Add(new SFFastLineSeries() {
                    XBindingPath = "Index",
                    YBindingPath = "Value",
                    LineWidth = 1,
                });
            }

            set.Bind(StartScanning)
                .To(vm => vm.ScanRequested);

			//Binding to series with data in viewmodel
			set.Bind(sFFastLineSeries[0])
		        .To(vm => vm.DataListString["DataType.ecg"])
		        .For(v => v.ItemsSource);
	        sFFastLineSeries[0].Color = UIColor.Brown;

			set.Bind(sFFastLineSeries[1])
               .To(vm => vm.DataListString["DataType.scg"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[1].Color = UIColor.Yellow;

            set.Bind(sFFastLineSeries[2])
               .To(vm => vm.DataListString["DataType.red"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[2].Color = UIColor.Red;

            set.Bind(sFFastLineSeries[3])
               .To(vm => vm.DataListString["DataType.ir"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[3].Color = UIColor.Purple;

            set.Bind(sFFastLineSeries[4])
	            .To(vm => vm.DataListString["DataType.chest_temp"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[4].Color = UIColor.Orange;

            set.Bind(sFFastLineSeries[5])
	            .To(vm => vm.DataListString["DataType.foot_temp"])
			   .For(v => v.ItemsSource);
            sFFastLineSeries[5].Color = UIColor.Green;

            set.Apply();

			//add series to graph
			EcgGraph.Series.Add(sFFastLineSeries[0]);
	        ScgGraph.Series.Add(sFFastLineSeries[1]);
	        RedGraph.Series.Add(sFFastLineSeries[2]);
	        IrGraph.Series.Add(sFFastLineSeries[3]);
	        ChestTempGraph.Series.Add(sFFastLineSeries[4]);
	        FootTempGraph.Series.Add(sFFastLineSeries[5]);
		}
    }
}
